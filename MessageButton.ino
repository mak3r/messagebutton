#include <Arduino.h>

#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino

//needed for library
#include <ESP8266WebServer.h>
#include <DNSServer.h>
#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager

//for LED status
#include <Ticker.h>
#include <vector>

#include "GroundWorks.h"
#include "SPSTDevice.h"
#include "TickTock.h"

#define TRIGGER_PIN 0
#define LED_BLUE 2

State curState;
State nextState;
SPSTDevice b1Device (TRIGGER_PIN);
GroundWorks groundWorks;
ButtonAction lastAction;
TickTock *blipBlip = NULL;
std::vector<unsigned long> shortPressTiming = {100,200,100,200};
std::vector<unsigned long> dblClickTiming = {300,500,300,500,2000,500};
std::vector<unsigned long> clickTiming = {1000,2000};
std::vector<unsigned long> setupDeviceTiming = {50,50};

void LEDAlert(int pin, std::vector<unsigned long> timing) {
  Serial.println("Creating alert starting with: " + String(timing[0]));
  if (blipBlip != NULL) {
    delete blipBlip;
  }
  //Seems like the Huzzah has the LED AT pin2 set to be off when pulled high
  blipBlip = new TickTock(pin, timing, HIGH);
  blipBlip->start();
}


void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  Serial.println("\n Starting");

  curState = STANDBY;
  nextState = STANDBY;

  pinMode(TRIGGER_PIN, INPUT);
}


void loop() {
  ButtonAction buttonAction = b1Device.getButtonAction();

  curState = nextState;
  nextState = TRANSITION;

  switch (curState) {
    case SETUP_DEVICE:
    {
      Serial.println("Entering setup b1Device state.");
      groundWorks.configureAccess();
      nextState = STANDBY;
      break;
    }
    case DEEP_SLEEP:
    {
      Serial.println("Entering deep sleep state.");
      // prepare to save

      //countdown to deep sleep

      //deep sleep
      break;
    }
    case RECORD_MESSAGE:
    {
      //Serial.println("Entering record message");
      if (buttonAction == DOUBLE_CLICK) {
        nextState = DELETE_LAST_MSG;
      } else if (buttonAction == CLICK) {
        blipBlip->stop();
        nextState = STANDBY;
      } else {
        nextState = RECORD_MESSAGE;
      }
      break;
    }
    case SEND_MESSAGE:
    {
      Serial.println("Entering send message state");
      nextState = STANDBY;
      break;
    }
    case CHECK_MESSAGES:
    {
      if (buttonAction == CLICK) {
        Serial.println("The button clicked and we're stopping the alert.");
        blipBlip->stop();
        nextState = STANDBY;
      } else {
        nextState = CHECK_MESSAGES;
      }
      break;
    }
    case STANDBY:
    {
      //Serial.println("Entering standby state.");
      if (buttonAction == LONG_PRESS) {
        nextState = SETUP_DEVICE;
        LEDAlert(LED_BLUE,setupDeviceTiming);
      } else if ( buttonAction == SHORT_PRESS) {
        nextState = CHECK_MESSAGES;
        LEDAlert(LED_BLUE,shortPressTiming);
      } else if ( buttonAction == DOUBLE_CLICK) {
        nextState = DELETE_LAST_MSG;
        LEDAlert(LED_BLUE,dblClickTiming);
      } else if ( buttonAction == CLICK ) {
        nextState = RECORD_MESSAGE;
        LEDAlert(LED_BLUE,clickTiming);
      } else if (buttonAction == NONE) {
        nextState = STANDBY;
      }
      break;
    }
    case DELETE_LAST_MSG:
    {
      //Serial.println("Entering delete last message");
      if (buttonAction == CLICK) {
        blipBlip->stop();
        nextState = STANDBY;
      } else {
        nextState = DELETE_LAST_MSG;
      }
      break;
    }
    case CANCEL_SETUP:
    {
      Serial.println("Entering cancel setup state.");
      nextState = STANDBY;
      break;
    }
    default:
    break;
  }// end switch

}//end loop
