#ifndef GroundWorks_h
#define GroundWorks_h

#include <WiFiManager.h>
#include <vector>
#include "TickTock.h"
#include "SPSTDevice.h"

void configModeCallback (WiFiManager *myWiFiManager);

class GroundWorks {
public:
  GroundWorks();
  ~GroundWorks();

  void configureAccess();

private:

};

#endif
