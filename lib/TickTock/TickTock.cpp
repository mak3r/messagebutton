#include "TickTock.h"

//Ticker needs a pointer to a non-member function whose return type is void
TickTock* tt;
void TickTockRunner()
{
  tt->toggle();
}

TickTock::TickTock(int pin, std::vector<unsigned long> sequence_ms) {
  TickTock(pin,sequence_ms,LOW);
}

TickTock::TickTock(int pin, std::vector<unsigned long> sequence_ms, int defaultState) {
  _pin = pin;
  _sequence_ms = sequence_ms;
  _defaultState = defaultState;
  _curIndex = 0;
  pinMode(_pin, OUTPUT);
  stop();
  tt=this;
}

TickTock::~TickTock() {
  _sequence_ms = std::vector<unsigned long>();
  tt=NULL;
}

void TickTock::start() {
  _isStarted = false;
  _ticker.attach_ms(_sequence_ms[_curIndex], TickTockRunner);
}

void TickTock::stop() {
  _ticker.detach();
  digitalWrite(_pin, _defaultState);
}

void TickTock::toggle() {
  int pinState = digitalRead(_pin);
  digitalWrite(_pin, !pinState);

  //If we're just starting return
  if (!_isStarted){
    _isStarted = true;
    _curIndex++;
    return;
  }

  //make the next call
  _ticker.attach_ms(_sequence_ms[nextIndex()], TickTockRunner);
}

int TickTock::nextIndex() {
  //Increment current index and check if it's still valid. Update to 0 if not
  if (++_curIndex >= _sequence_ms.size()) {
    _curIndex = 0;
  }
  return _curIndex;
}
