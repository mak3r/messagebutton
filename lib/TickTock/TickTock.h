#ifndef TickTock_h
#define TickTock_h

#include <Arduino.h>
#include <Ticker.h>
#include <vector>

// A class to quickly produce output alerts given a repeating sequence
// of HIGH/LOW states
// The ctor takes a vector of unsigned long elements which define
// the HIGH/LOW sequence beginning with the first duration being the
// opposite of the default setting
// When the last duration is reached, the sequence begins again
// Call start() to begin the sequence and stop() to immeidately return
// the sequence to it's default state
class TickTock {

public:

  // Create a new TickTock for pinout pin
  // using the on/off sequence sequence_ms
  // setting the default pin state to LOW
  TickTock(int pin, std::vector<unsigned long> sequence_ms);
  // Create a new TickTock for pinout pin
  // using the on/off sequence sequence_ms
  // setting the default pin state to defaultState
  TickTock(int pin, std::vector<unsigned long> sequence_ms, int defaultState);
  ~TickTock();

  void start();
  void stop();
  void toggle();

private:
  Ticker _ticker;
  int _pin;
  bool _isStarted;
  std::vector<unsigned long> _sequence_ms;
  int _defaultState;
  int _curIndex;
  int nextIndex();
};

void TickTockRunner();

#endif
