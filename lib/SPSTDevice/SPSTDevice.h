#ifndef SPSTDevice_h
#define SPSTDevice_h

#include <Arduino.h>

#ifndef _DEBUG
//#define _DEBUG 1
#endif

#ifdef _DEBUG
const char DBG_CLICK[] = "CLICK";
const char DBG_DOUBLE_CLICK[] = "DOUBLE_CLICK";
const char DBG_LONG_PRESS[] = "LONG_PRESS";
const char DBG_SHORT_PRESS[] = "SHORT_PRESS";
const char DBG_NONE[] = "NONE";
#endif

//Duration the button will be pressed to be considered a short press
const unsigned long SHORT_PRESS_DUR = 5000;
//Duration the button will be pressed to be considered a long press
const unsigned long LONG_PRESS_DUR = 10000;
//Duration that can pass to indicate a click
const unsigned long CLICK_DUR = 250;
//Longest Duration between two clicks to consider it a double click
const unsigned long DOUBLE_CLICK_BETWEEN = 175;

//Number of cycles to use when getting the avg button state
const int BOUNCE_COUNT_MAX = 10;

// Device enumerates the possible states that the device may be in.
enum State {
  DEEP_SLEEP,
  RECORD_MESSAGE,
  SEND_MESSAGE,
  CHECK_MESSAGES,
  STANDBY,
  SETUP_DEVICE,
  DELETE_LAST_MSG,
  CANCEL_SETUP,
  TRANSITION = -1
};

// The current button action
enum ButtonAction {
  LONG_PRESS,
  SHORT_PRESS,
  DOUBLE_CLICK,
  CLICK,
  NONE
};

class SPSTDevice {
  public:

    SPSTDevice(int pin);

    ButtonAction getButtonAction();
    //The previous ButtonAction
    const ButtonAction& prevButtonAction() const { return _prevButtonAction; }
    //The duration between the previous button action and the current button action
    const unsigned long& millisHIGH() const { return _durMillisHIGH; }

  private:
    //pin that the spst button is connected to
    int _pin;
    //Used to count how many times the button is low up to BOUNCE_COUNT_MAX and then it is reset
    int _btnStateCounter;
    //The agregated state of the button
    int _buttonState;
    //The previous aggregated state of the button
    int _prevButtonState;
    //A button state sequence dictates the current action
    ButtonAction _curButtonAction;
    //Keep track of the previous action it may effect the next curButtonAction
    ButtonAction _prevButtonAction;
    //An array to keep track of the button states until it's time to aggregate them
    int _buttonCheck[BOUNCE_COUNT_MAX];

    //duration that state was low (triggered by a button state change to HIGH)
    unsigned long _durMillisLOW;
    //duration that state was HIGH (triggered by a button state change LOW)
    unsigned long _durMillisHIGH;

    //record a LOW value when the button state change has gone HIGH
    unsigned long _lastMillisLOW;
    //record a HIGH value when the button state change has gone LOW
    unsigned long _lastMillisHIGH;

    //milliseconds that elapsed since the last CLICK action was recorded (possibly indicating a double click)
    unsigned long _btwnClickMillis;
    //records whether the button state has changed
    bool _buttonStateChanged;


    ButtonAction evalButtonAction();
    // Keeps a running tally of the current state of the trigger pin
    int avgButtonState();
    // Average (mean) of the values in the array from 0 to numVals
    int mean(int vals[], int numVals);
    //General debugging
    template <typename Generic> void DEBUG_DS(Generic text);
    //Debug button actions
    void DEBUG_DS(ButtonAction action);

};

#endif
