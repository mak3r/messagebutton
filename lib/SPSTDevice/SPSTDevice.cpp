#include "SPSTDevice.h"

SPSTDevice::SPSTDevice(int pin) {
  _pin = pin;
  _btnStateCounter = 0;
  _buttonState = HIGH;
  _prevButtonState = HIGH;
  _curButtonAction = NONE;
  _prevButtonAction = NONE;
  _buttonStateChanged = false;
  _lastMillisHIGH = millis();
  _lastMillisLOW = millis();
  _durMillisLOW = 0;
  _durMillisHIGH = 0;
}

template <typename Generic> void SPSTDevice::DEBUG_DS(Generic text) {
  #ifdef _DEBUG
  if(_DEBUG) {
    Serial.print("*DS: ");
    Serial.println(text);
  }
  #endif
}

void SPSTDevice::DEBUG_DS(ButtonAction action) {
  #ifdef _DEBUG
  if (_DEBUG) {
    Serial.print("*DS: ");
    Serial.print(String(action) + ":");
    switch (action) {
      case NONE:
      {
        Serial.println(DBG_NONE);
        break;
      }
      case DOUBLE_CLICK:
      {
        Serial.println(DBG_DOUBLE_CLICK);
        break;
      }
      case CLICK:
      {
        Serial.println(DBG_CLICK);
        break;
      }
      case SHORT_PRESS:
      {
        Serial.println(DBG_SHORT_PRESS);
        break;
      }
      case LONG_PRESS:
      {
        Serial.println(DBG_LONG_PRESS);
        break;
      }
      default:
      break;
    }
  }
  #endif
}


ButtonAction SPSTDevice::getButtonAction() {
  ButtonAction returnAction = evalButtonAction();

  if ( returnAction == CLICK) {
    if (_prevButtonAction == CLICK
      && _durMillisHIGH < DOUBLE_CLICK_BETWEEN) {
        returnAction = DOUBLE_CLICK;
      } else if (_durMillisHIGH > DOUBLE_CLICK_BETWEEN) {
        returnAction = CLICK;
      }
    }
    #ifdef _DEBUG
    if (_buttonStateChanged) {
      DEBUG_DS(returnAction);
    }
    #endif

    _curButtonAction = returnAction;
    return returnAction;
  }

  ButtonAction SPSTDevice::evalButtonAction() {

    int buttonState = avgButtonState();
    ButtonAction buttonAction = _curButtonAction;

    unsigned long curMillis = millis();
    if ( buttonState != _prevButtonState ) {
      _buttonStateChanged = true;
    } else {
      _buttonStateChanged = false;
    }
    _prevButtonState = buttonState;


    //Don't evaluate the button action until the state returns to high
    if ( buttonState == HIGH ) {
      if ( _buttonStateChanged ) {
        //mark the current duration of the low state
        _durMillisLOW = curMillis - _lastMillisLOW;
        _lastMillisHIGH = curMillis;
        #ifdef _DEBUG
        DEBUG_DS("_durMillisLOW:\t\t" + String(_durMillisLOW));
        #endif
        if (_durMillisLOW > LONG_PRESS_DUR) {
          buttonAction = LONG_PRESS;
        } else if (_durMillisLOW > SHORT_PRESS_DUR) {
          buttonAction = SHORT_PRESS;
        } else if (_durMillisLOW > 0 && _durMillisLOW < CLICK_DUR) {
          buttonAction = CLICK;
        } else {
          buttonAction = NONE;
        }

      } else {
        buttonAction = NONE;
      }
      if (_curButtonAction != NONE) {
        //Capture the current button action of interest before it get's set to NONE
        _prevButtonAction = _curButtonAction;
      }

    }

    if ( buttonState == LOW ) {
      if ( _buttonStateChanged ) {
        _durMillisHIGH = curMillis - _lastMillisHIGH;
        _lastMillisLOW = curMillis;
        #ifdef _DEBUG
        DEBUG_DS("_durMillisHIGH:\t\t" + String(_durMillisHIGH));
        #endif
      }
    }

    return buttonAction;
  }

  int SPSTDevice::avgButtonState() {
    if (_btnStateCounter < BOUNCE_COUNT_MAX) {
      _buttonCheck[_btnStateCounter++] = digitalRead(_pin);
    }
    else {
      //reset the button state counter
      _btnStateCounter = 0;
      //Find out the average of the recent batch
      _buttonState = mean(_buttonCheck, BOUNCE_COUNT_MAX);
      //reset the array
      for (int i = 0; i<BOUNCE_COUNT_MAX; i++) {
        _buttonCheck[i] = 0;
      }
    }
    return _buttonState;
  }

  int SPSTDevice::mean(int *vals, int numVals) {
    int sum = 0;
    for(int i=0; i<numVals; i++) {
      sum += (vals[i] * 10);
    }
    int avg = (sum + 5)/(numVals*10);
    return avg;
  }
